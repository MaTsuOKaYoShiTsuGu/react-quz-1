import React from 'react'
import './style/guessCard.less'
import './style/button.less'

export default class GuessCard extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            guessAnswer : []
        };
        this.inputOnChange = this.inputOnChange.bind(this);
        this.judgeAnswer = this.judgeAnswer.bind(this);
    }

    inputOnChange(event){
        this.props.setGuessAnswer(event.target.value);
        this.setState({
            guessAnswer : event.target.value
        });
    }

    judgeAnswer(){
        this.props.judgeAnswer(this.state.guessAnswer.slice(0,4));
    }

    render() {
        return (
            <div className='guess-card'>
                <p>guess card</p>
                <input onChange={this.inputOnChange}/>
                <br/>
                <button onClick={this.judgeAnswer}>Guess</button>
            </div>
        );
    }
}