import React from 'react'
import GuessCard from "./guessCard";
import InputField from "./InputField";
import {Fail, Success} from './result'
import './style/playingPanel.less'

export default class PlayingPanel extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            guessAnswer: [],
            showResult: false,
            result: "FAILED"
        };
        this.setGuessAnswer = this.setGuessAnswer.bind(this);
        this.judgeAnswer = this.judgeAnswer.bind(this);
    }

    setGuessAnswer(guessAnswer) {
        this.setState({
            guessAnswer: guessAnswer
        });
    }

    judgeAnswer(answer) {
        this.setState({
            showResult: true,
            result: this.props.answer === answer
        });
    }

    render() {
        return (
            <div className='playing-panel'>
                <div>
                    playingPanel
                    <InputField answer={this.state.guessAnswer}/>
                    <div className='result'>
                        {this.state.showResult ? this.state.result ? <Success/> : <Fail/> : null}
                    </div>
                </div>
                <GuessCard setGuessAnswer={this.setGuessAnswer} judgeAnswer={this.judgeAnswer}
                           clearData={this.props.clearData} shouldClearData={this.props.shouldClearData}/>
            </div>
        );
    }
}