import React from 'react'
import InputField from "./InputField";
import './style/controlPanel.less';
import './style/button.less'

export default class ControlPanel extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            showAnswer : true
        };
        this.generateNewGame = this.generateNewGame.bind(this);
        this.onShowResult = this.onShowResult.bind(this);
    }

    onShowResult(){
        this.setState({
            showAnswer : true
        });
    }
    render() {
        return (
            <div className='control-panel'>
                <h2>New Card</h2>
                <button className='generate-game' onClick={this.generateNewGame}>New Card</button>
                <InputField answer={this.state.showAnswer?this.props.answer:[]} />
                <button className='show-result' onClick={this.onShowResult}>Show Result</button>
            </div>
        );
    }

    generateNewGame(){
        let charArray = [4];
        for (let i=0;i<4;i++){
            charArray[i]=String.fromCharCode(Math.random()*26+65);
        }
        this.props.getAnswer(charArray.join(""));
        this.props.clearData(true);
        setTimeout(()=>{
            this.props.clearData(false);
        },1000);
        this.setState({
            showAnswer: true
        });
        setTimeout(()=>{
            this.setState({
                showAnswer: false
            })
        },3000);
    }
}