import React from 'react'
import './style/inputField.less'

export default class InputField extends React.Component{

    constructor(props, context) {
        super(props, context);
    }

    render() {
        let answer = (String)(this.props.answer);
        return (
            <div className='input-field'>
                <input readOnly={true} value={answer.charAt(0)}/>
                <input readOnly={true} value={answer.charAt(1)}/>
                <input readOnly={true} value={answer.charAt(2)}/>
                <input readOnly={true} value={answer.charAt(3)}/>
            </div>
        );
    }
}
