import React from 'react'
import './style/result.less'

const Success = () => {
    return <div className="success">SUCCESS</div>;
};

const Fail = () => {
    return <div className="failed">FAILED</div>
};

export {Success,Fail}