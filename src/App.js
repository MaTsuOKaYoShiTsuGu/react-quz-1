import React from 'react'
import PlayingPanel from "./playingPanel";
import ControlPanel from "./controlPanel";
import './style/App.less';

export default class App extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            answer: [],
            clearData : false
        };
        this.getAnswer = this.getAnswer.bind(this);
        this.clearData = this.clearData.bind(this);
    }

    clearData(clearData){
        this.setState({
            clearData: clearData
        });
    }

    getAnswer(answer){
        this.setState({
            answer: answer
        });
    }

    render() {
        return (
            <div className='app'>
                <PlayingPanel answer={this.state.answer} clearData={this.clearData} shouldClearData={this.state.clearData}/>
                <ControlPanel getAnswer={this.getAnswer} answer={this.state.answer} clearData={this.clearData}/>
            </div>
        );
    }
}